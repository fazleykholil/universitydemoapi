﻿namespace UniversityModels
{
    public class Student : Person
    {
        public string course { get; set; }
        public int year { get; set; }
        public int LecturerId { get; set; }
    }
}