﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using UniversityApi.Utils;
using UniversityModels;

namespace UniversityApi.Models
{
    public class StudentRepo : IRepo
    {
        public static List<Person> PersonList = new List<Person>();

        public IEnumerable<Person> Get()
        {
            SqlDataReader reader = null;
            DbConnection dbConnection = new DbConnection();
            dbConnection
                .initConnection()
                .setSqlCommand(CommandType.StoredProcedure, "sp_get_students");
            reader = dbConnection.SqlCmd.ExecuteReader();
            while (reader.Read())
            {
                yield return (Student)ObjectMapper.ConvertReaderToObj<Student>(reader);
            }
            dbConnection.MyConnection.Close();
        }

        public Person Get(int id)
        {
            return Get().SingleOrDefault(i => i.Id == id);
        }

        public Person Add(Person person)
        {
            Student student = (Student)person;
            using (SqlConnection sqlConnection = new SqlConnection(DbConnection.connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand())
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Connection = sqlConnection;
                    sqlCommand.CommandText = "sp_add_student";
                    sqlCommand.Parameters.AddWithValue("@FirstName", student.FirstName);
                    sqlCommand.Parameters.AddWithValue("@LastName", student.LastName);
                    sqlCommand.Parameters.AddWithValue("@Age", student.Age);
                    sqlCommand.Parameters.AddWithValue("@Address", student.Address);
                    sqlCommand.Parameters.AddWithValue("@course", student.course);
                    sqlCommand.Parameters.AddWithValue("@year", student.year);
                    sqlCommand.Parameters.AddWithValue("@LecturerId", student.LecturerId);
                    sqlConnection.Open();
                    sqlCommand.ExecuteNonQuery();
                    sqlConnection.Close();
                    return (Person)student;
                }
            }
        }

        public bool Put(int id, Person person)
        {
            Student student = (Student)person;
            using (SqlConnection sqlConnection = new SqlConnection(DbConnection.connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand())
                {
                    try
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandText = "sp_update_student";
                        sqlCommand.Parameters.AddWithValue("@Id", id);
                        sqlCommand.Parameters.AddWithValue("@FirstName", student.FirstName);
                        sqlCommand.Parameters.AddWithValue("@LastName", student.LastName);
                        sqlCommand.Parameters.AddWithValue("@Age", student.Age);
                        sqlCommand.Parameters.AddWithValue("@Address", student.Address);
                        sqlCommand.Parameters.AddWithValue("@course", student.course);
                        sqlCommand.Parameters.AddWithValue("@year", student.year);
                        sqlCommand.Parameters.AddWithValue("@LecturerId", student.LecturerId);
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                        sqlConnection.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
        }

        public bool Delete(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(DbConnection.connectionString))
            {
                using (SqlCommand sqlCommand = new SqlCommand())
                {
                    try
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandText = "sp_delete_student";
                        sqlCommand.Parameters.AddWithValue("@Id", id);
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                        sqlConnection.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
        }
    }
}