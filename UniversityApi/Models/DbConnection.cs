﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace UniversityApi.Models
{
    public class DbConnection
    {
        public static string connectionString = "Server=PCCKOMU001;Database=StudentMgt;User ID=administration_user;Password=administration_user;";

        public SqlConnection MyConnection { get; set; }

        public SqlCommand SqlCmd { get; set; }

        public DbConnection initConnection()
        {
            this.MyConnection = new SqlConnection();
            this.MyConnection.ConnectionString = DbConnection.connectionString;
            return this;
        }

        public DbConnection setReader(SqlDataReader reader)
        {
            return this;
        }

        public DbConnection setSqlCommand(CommandType type, string query)
        {
            this.SqlCmd = new SqlCommand();
            this.SqlCmd.CommandType = type;
            this.SqlCmd.CommandText = query;
            this.SqlCmd.Connection = this.MyConnection;
            this.MyConnection.Open();
            return this;
        }
    }
}