﻿using System.Collections.Generic;
using UniversityModels;

namespace UniversityApi.Models
{
    public interface IRepo
    {
        IEnumerable<Person> Get();

        Person Get(int id);

        Person Add(Person person);

        bool Put(int id, Person person);

        bool Delete(int id);
    }
}
