﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Microsoft.Practices.Unity;
using UniversityApi.Controllers;
using UniversityApi.Models;
using UniversityApi.Handlers;

namespace UniversityApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.MessageHandlers.Add((DelegatingHandler)new LoggingHandler());
            config.MessageHandlers.Add((DelegatingHandler)new ApiKeyHandler("uqyw1-iwodk-001ok-orplo-09ikj"));

            IUnityContainer objContainer = new UnityContainer();
            objContainer.RegisterType<StudentController>();
            objContainer.RegisterType<IRepo, StudentRepo>();
            config.DependencyResolver = new UnityResolver(objContainer);

        }
    }
}
