﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace UniversityApi.Utils
{
    public class ObjectMapper
    {
        public static object ConvertReaderToObj<T>(IDataReader reader)
        {
            if (!typeof(T).IsClass)
                throw new NotSupportedException("Type has to be POCO class");

            var result = Activator.CreateInstance(typeof(T));

            foreach (PropertyInfo item in result.GetType().GetProperties())
            {
                object value;
                try
                {
                    value = reader[item.Name];
                }
                catch
                {
                    continue;
                }

                if (value == DBNull.Value) continue;

                var map = Convert.ChangeType(value, item.PropertyType);
                item.SetValue(result, map);
            }
            return result;
        }
    }
}