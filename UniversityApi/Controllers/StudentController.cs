﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UniversityApi.Models;
using UniversityModels;

namespace UniversityApi.Controllers
{

    [RoutePrefix("api")]
    public class StudentController : ApiController
    {
        private IRepo studentRepo;

        public StudentController(IRepo repo)
        {
            studentRepo = repo;
        }

        [Route("students")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, this.studentRepo.Get());
        }

        [Route("students")]
        [HttpGet]
        public Person Get([FromUri] int id)
        {
            return this.studentRepo.Get(id);
        }

        [Route("students")]
        [HttpPost]
        public HttpResponseMessage Post(Student student)
        {
            try
            {
                if (this.ModelState.IsValid)
                    return Request.CreateResponse(HttpStatusCode.Created, this.studentRepo.Add((Person)student));
            }
            catch (Exception ex)  
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError);
        }

        [Route("students")]
        [HttpPut]
        public HttpResponseMessage Put(int id, Student student)
        {
            return Request.CreateResponse( HttpStatusCode.OK, this.studentRepo.Put(id,student));
        }

        [Route("students")]
        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, this.studentRepo.Delete(id));
        }
    }
}