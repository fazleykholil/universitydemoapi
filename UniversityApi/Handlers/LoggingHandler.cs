﻿using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using UniversityApi.Models;

namespace UniversityApi.Handlers
{
    public class LoggingHandler : DelegatingHandler
  {
    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
      HttpResponseMessage httpResponseMessage = await base.SendAsync(request, cancellationToken);
      HttpResponseMessage response = httpResponseMessage;
      httpResponseMessage = (HttpResponseMessage) null;
      SqlConnection cn = new SqlConnection(DbConnection.connectionString);
      try
      {
        SqlCommand cmd = new SqlCommand();
        try
        {
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Connection = cn;
          cmd.CommandText = "sp_add_log";
          cmd.Parameters.AddWithValue("@Method", (object) request.Method.ToString());
          cmd.Parameters.AddWithValue("@Uri", (object) request.RequestUri.ToString());
          cmd.Parameters.AddWithValue("@RequestMethod", (object) request.Method.ToString());
          cmd.Parameters.AddWithValue("@StatusCode", (object) response.StatusCode);
          cn.Open();
          cmd.ExecuteNonQuery();
          cn.Close();
        }
        finally
        {
          if (cmd != null)
            cmd.Dispose();
        }
        cmd = (SqlCommand) null;
      }
      finally
      {
        if (cn != null)
          cn.Dispose();
      }
      cn = (SqlConnection) null;
      return response;
    }
  }
}