﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace UniversityApi.Handlers
{
    public class ApiKeyHandler : DelegatingHandler
    {
        public string Key { get; set; }
 
        public ApiKeyHandler(string key)
        {
            this.Key = key;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (this.ValidateKey(request))
                return base.SendAsync(request, cancellationToken);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.Forbidden);
            TaskCompletionSource<HttpResponseMessage> completionSource = new TaskCompletionSource<HttpResponseMessage>();
            completionSource.SetResult(result);
            return completionSource.Task;
        }

        private bool ValidateKey(HttpRequestMessage message)
        {
            return Enumerable.FirstOrDefault<string>(message.Headers.GetValues("verification-token")).ToString() == this.Key;
        }
    }
}